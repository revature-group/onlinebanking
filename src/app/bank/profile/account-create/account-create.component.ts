import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/user.service';
import { ToastrService } from 'ngx-toastr';
import { Client } from 'src/app/models/Client';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account-create',
  templateUrl: './account-create.component.html',
  styleUrls: ['./account-create.component.css']
})
export class AccountCreateComponent implements OnInit {

  client!: Client;
  formModel: any;
  // client: Client[] = [];

  constructor(public service: UserService,  private toastr: ToastrService, private Router: Router) { }

  ngOnInit(): void {
    this.service.updateToken().subscribe(res =>{
      //localStorage.setItem('token', null);
      localStorage.setItem('token', JSON.stringify(res));
      this.client =  JSON.parse(localStorage.getItem('token'));
    });
    this.client =  JSON.parse(localStorage.getItem('token'));
  }


  onSubmit() {
    this.service.createNewAccount().subscribe(
      (res: any) => {

        if (res.succeded) {
          this.service.formModel.reset();
          this.toastr.success('New User Submitted', 'Registration Successful.');
        } else {
          this.toastr.error("account cannot created", 'Registration failed');
        }
      },
      err => {
        // console.log(err);
      }
    );
    this.service.updateToken().subscribe(res =>{
      // console.log(res);
      localStorage.setItem('token', null);
      localStorage.setItem('token', JSON.stringify(res));
      this.client =  JSON.parse(localStorage.getItem('token'));
      // console.log(this.client);
    });
  }

}
