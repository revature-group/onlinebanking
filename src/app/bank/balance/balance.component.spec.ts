import { ComponentFixture, TestBed } from '@angular/core/testing';
 import { UserService } from 'src/app/shared/user.service';
import {RouterTestingModule} from '@angular/router/testing'

import { BalanceComponent } from './balance.component';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('BalanceComponent', () => {
  let component: BalanceComponent;
  let fixture: ComponentFixture<BalanceComponent>;
  let router: Router;
  let service: UserService
  let fb : FormBuilder;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([]), ReactiveFormsModule, HttpClientTestingModule],
      declarations: [ BalanceComponent ],
      providers: [UserService]
    
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BalanceComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    service = TestBed.inject(UserService);
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
