export interface AccountStatement {
    transactionId:number;
    transactionDateTime:Date;
    description:string;
    creditAmount:string;
    debitAmount:string;
    currentBalance:string;
}