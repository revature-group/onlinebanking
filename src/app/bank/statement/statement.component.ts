import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/models/Client';
import { UserService } from 'src/app/shared/user.service';
import {AccountStatement} from "./statement";



@Component({
  selector: 'app-statement',
  templateUrl: './statement.component.html',
  styleUrls: ['./statement.component.css']
})
export class StatementComponent implements OnInit {
  pageTitle = "Account Statement";
  
  accountId: string = '';
  client!: Client;

  statement:AccountStatement[] = [];
  formModel: any;

  constructor(private userService:UserService) { }
 
  ngOnInit(): void {
    this.userService.updateToken().subscribe(res =>{
      //localStorage.setItem('token', null);
      localStorage.setItem('token', JSON.stringify(res));
      this.client =  JSON.parse(localStorage.getItem('token'));
    });
    this.client =  JSON.parse(localStorage.getItem('token'));
  }

  submitRequest(){
    let html = <HTMLInputElement>document.getElementById('accountNumber');
    this.accountId = html.value;
    // console.log(this.accountId)
    this.userService.getStatement(this.accountId)
      .subscribe(res => {
        // console.log(res);
        this.statement = [...res];
      });
  }
}
