import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from 'src/app/shared/user.service';

import { StatementComponent } from './statement.component';

describe('StatementComponent', () => {
  let component: StatementComponent;
  let fixture: ComponentFixture<StatementComponent>;
  let router: Router;
  let service: UserService
  let fb : FormBuilder;
  let serviceSpy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatementComponent ],
      imports: [RouterTestingModule.withRoutes([]), ReactiveFormsModule, HttpClientTestingModule],
      providers: [UserService]
    })
    .compileComponents();
  });

  const dummyData = {
    account: "132548102"
  }

  beforeEach(() => {
    fixture = TestBed.createComponent(StatementComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    service = TestBed.inject(UserService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call the view statement method', ()=>{
    serviceSpy = spyOn(service, `getStatement`);
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    
    fixture.detectChanges();

    expect(serviceSpy).toHaveBeenCalled();

  });

});
