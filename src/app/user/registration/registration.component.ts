import { Component, OnInit, ViewChild} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/shared/user.service';
import { Router } from '@angular/router';

import { PhotoUploadComponent }  from './photo-upload/photo-upload.component';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit{

  @ViewChild(PhotoUploadComponent) photoUpload:PhotoUploadComponent;
  constructor(public service: UserService, private router: Router, private toastr: ToastrService) {
  }


  ngOnInit(): void {
    this.service.formModel.reset();
  }
  onSubmit() {
    console.log('i am submitting in register on submit');
    this.service.register(this.photoUpload.fileName).subscribe(
      (res: any) => {
        console.log("console printed " + res)
        if (res == "resource was created") {
          this.photoUpload.upload();
          this.photoUpload.selectFile(null);
          this.service.formModel.reset();
          this.photoUpload.resetPhotoField();
          // this.photoUpload.uploadinput.nativeElement.value = null;
          this.toastr.success('New User Submitted', 'Registration Successful.');
          this.router.navigateByUrl('/user/login');
        } else {
          res.errors.forEach((element: { code: any; description: any }) => {
            switch (element.code) {
              case 'DuplicateUser':
                this.toastr.error("Username is already taken", 'Registration failed');
                break;
              default:
                this.toastr.error(element.description, 'Registration failed');
                break;
            }
          });
        }
      },
      err => {
        alert("username is taken");
        console.log(err);
      }
    );
  }

  validateFirstName(){
    return this.service.firstName?.invalid && !this.service.firstName?.untouched;
  }

  validateLastName(){
    return this.service.lastName?.invalid && !this.service.lastName?.untouched;
  }

  validateAddress(){
    return this.service.address?.invalid && !this.service.address?.untouched;
  }

  validateAge(){
    return this.service.age?.invalid && !this.service.age?.untouched;
  }

  validateUsername(){
    return this.service.username?.invalid && !this.service.username?.untouched;
  }

  validateEmail(){
    return this.service.email?.invalid && !this.service.email?.untouched;
  }

  validatePassword(){
    return this.service.password?.invalid && !this.service.password?.untouched;
  }

  validateButton(){
    return this.service.formModel.invalid;
  }

  validateCheckBox(){
    return this.service.agree?.invalid;
  }

  validateContactNumber(){
    return this.service.contactNumber?.invalid && !this.service.contactNumber?.untouched;
  }

  validateContactNumberExample(){
    return this.service.contactNumber?.invalid;
  }


};

