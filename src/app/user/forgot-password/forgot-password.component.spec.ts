import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/shared/user.service';

import { ForgotPasswordComponent } from './forgot-password.component';

describe('ForgotPasswordComponent', () => {
  let component: ForgotPasswordComponent;
  let fixture: ComponentFixture<ForgotPasswordComponent>;
  let service:UserService;
  let mockClient:{get: jasmine.Spy, post:jasmine.Spy, put:jasmine.Spy, delete: jasmine.Spy}
  let serviceSpy;
  

const dummyEmailData = {
  email: 'starjohnson@maildrop.cc'
};
TestBed.configureTestingModule({
  declarations: [ForgotPasswordComponent],
  imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule],
  providers: [
    {provide: UserService, useClass: serviceSpy},
    {provide: HttpClient, useValue: mockClient}
  ]
})

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ForgotPasswordComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule],
      providers: [
        {provide: UserService},
        {provide: HttpClient}
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(ForgotPasswordComponent);
    component = fixture.componentInstance;
    
    fixture.detectChanges();

    service = TestBed.inject(UserService);
    mockClient = TestBed.get(HttpClient);

  });

  it('should create', () => {
    
    expect(component).toBeTruthy();
  });
  it('should call forgotPassword', () =>{
    fixture.componentInstance.forgotPassForm.email = dummyEmailData.email;
    serviceSpy = spyOn(service, `forgotPassword`);
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    
    fixture.detectChanges();

    expect(serviceSpy).toHaveBeenCalled();
  });
});
