import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule, routingComponents, userRouting } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { UserComponent } from './user/user.component';
import { LoginComponent } from './user/login/login.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { UserService } from './shared/user.service';
import {PhotoUploadService} from './user/registration/Services/photo-upload.service'
import { HttpClientModule} from '@angular/common/http';
import { BankComponent } from './bank/bank.component';
import { ProfileUpdateComponent } from './bank/profile/profile-update/profile-update.component';
import { AccountCreateComponent } from './bank/profile/account-create/account-create.component';
import { ViewProfileComponent } from './bank/profile/view-profile/view-profile.component';
import { CloseAccountComponent } from './bank/profile/close-account/close-account.component';
import { ForgotPasswordComponent } from './user/forgot-password/forgot-password.component';
import { PhotoUploadComponent } from './user/registration/photo-upload/photo-upload.component';
import { DepositFormComponent } from './bank/deposit/deposit-form/deposit-form.component';
import { WithdrawComponent } from './bank/withdraw/withdraw.component';
import { withdrawservice } from './bank/withdraw/withdrawservice.service';
import { DepositserviceService } from './bank/deposit/depositservice.service';




@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    userRouting,
    HomeComponent,
    AdminPanelComponent,
    ForbiddenComponent,
    UserComponent,
    LoginComponent,
    RegistrationComponent,
    BankComponent,
    ProfileUpdateComponent,
    AccountCreateComponent,
    ViewProfileComponent,
    CloseAccountComponent,
    ForgotPasswordComponent,
    PhotoUploadComponent,
    DepositFormComponent,
    WithdrawComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [UserService, PhotoUploadService, withdrawservice, DepositserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
