import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from 'src/app/models/Client';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private fb: FormBuilder, private http: HttpClient) { }
  readonly BaseURI = 'http://localhost:9030/api'

  accModal = this.fb.group({
    accountType:['', [Validators.required]],
    branch:['', [Validators.required]]
  })

  formModel = this.fb.group({
    firstName:['', [Validators.required]],
    lastName:['', [Validators.required]],
    address:['',[Validators.required]],
    age:['', [Validators.required]],
    username:['' ,[Validators.required]],
    password:['', [Validators.required, Validators.minLength(6)]],
    email:['', [Validators.required, Validators.email]],
    agree:[false, [Validators.requiredTrue]],
    contactNumber:['',  [Validators.required, Validators.minLength(7), Validators.maxLength(15), Validators.pattern("[0-9]{3}-[0-9]{3}-[0-9]{4}")]],
    // fileName:['',Validators.required]
  });



  get firstName(){
    return this.formModel.get('firstName');
  }
  get lastName(){
    return this.formModel.get('lastName');
  }
  get address(){
    return this.formModel.get('address');
  }
  get age(){
    return this.formModel.get('age');
  }
  get username(){
    return this.formModel.get('username');
  }
  get password(){
    return this.formModel.get('password');
  }
  get email(){
    return this.formModel.get('email');
  }
  get accountType(){
    return this.formModel.get('accountType');
  }
  get branch(){
    return this.formModel.get('branch');
  }
  get agree(){
    return this.formModel.get('agree');
  }
  get contactNumber(){
    return this.formModel.get('contactNumber');
  }

  register(fileName:any){
    let photoUrl: String = '';
    if(fileName != undefined){
      photoUrl = "https://photo-testing1.s3.us-east-2.amazonaws.com/" + fileName
    }
    let body = {
      firstName:this.formModel.value.firstName,
      lastName:this.formModel.value.lastName,
      address:this.formModel.value.address,
      age:this.formModel.value.age,
      username:this.formModel.value.username,
      password:this.formModel.value.password,
      email:this.formModel.value.email,
      accountType:this.formModel.value.accountType,
      branch:this.formModel.value.branch,
      contactNumber: this.formModel.value.contactNumber,
      photoUrl: photoUrl
    };
    // console.log(body)
    return this.http.post(this.BaseURI+'/users/register', body, {responseType: 'text'});
  }
  
  forgotPassword(formData): Observable<Object> {
    return this.http.post(this.BaseURI + "/users/forgotpassword", formData);
  }
  login(formData: any) {
    // console.log(formData);
    return this.http.post(this.BaseURI + '/users/login', formData);
  }
  getStatement(account: string) {
    return this.http.get<any>(`${this.BaseURI}/transaction/view?account=${account}`)
  }
  fundtransfer(acc1: String, acc2: String, amount: String): Observable<Object> {
    var body = {
      acc1,
      acc2,
      amount
    }
    // console.log(body);
    return this.http.post(this.BaseURI + '/bank/transfer', body)
  }
  updateProfile() {
    this.client = JSON.parse(localStorage.getItem('token'));
    var body = {
      firstName: this.formModel.value.firstName,
      lastName: this.formModel.value.lastName,
      address: this.formModel.value.address,
      contactNumber: this.formModel.value.contactNumber,
      email: this.formModel.value.email,
      clientId: this.client.clientId
    };
    // console.log(body)
    return this.http.put(this.BaseURI + '/users/bank/profile/updateprofile', body);
  }
  client!: Client;
  createNewAccount() {
    this.client = JSON.parse(localStorage.getItem('token'));
    var body = {
      accountType: this.accModal.value.accountType,
      branch: this.accModal.value.branch,
      clientId: this.client.clientId
    };
    return this.http.post(this.BaseURI + '/bank/profile/createaccount', body);
  }
  deleteAccount(accountnum) {
   //console.log(accountnum);
    this.client = JSON.parse(localStorage.getItem('token'));
  
    var body = {
      account_number : accountnum,
      email: this.client.email
    };
    // console.log(body)
    return this.http.post(this.BaseURI + '/transaction/delete', body);
  }
  updateToken() {
    this.client = JSON.parse(localStorage.getItem('token'));
    // console.log('updating token');
    var body = {
      username: this.client.username,
      password: this.client.password
    }
    return this.http.post(this.BaseURI + '/users/updateToken', body);
  }
}